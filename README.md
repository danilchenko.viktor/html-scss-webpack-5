# Boilerplate simple static HTML project with SCSS and Webpack 5

Fast start to create static html pages with scss preprocessor. Boilerplate is using Webpack 5 for development and build.

## Installation

Use the package manager [npm] or [yarn].

```bash
npm install
```

## Build project

```bash
npm run build
```

## Development

```bash
npm start
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
