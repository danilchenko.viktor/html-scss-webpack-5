const path = require('path');

const MiniCss = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  entry: './src/app.js',
  output: {
    path: __dirname + '/public/',
    filename: "app.js",
    publicPath: '/',
    clean: true,
  },
  module: {
    rules: [
      // Styles
      {
        test: /\.(s*)css$/,
        use: [MiniCss.loader, 'css-loader', 'sass-loader'],
      },
      {
        // Images
        test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/,
        type: "asset/resource",
      },
      {
        // Fonts
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        type: "asset/resource",
      },
    ],
  },
  plugins: [
    new MiniCss({ filename: "[name].css" }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: 'src/assets/images', to: 'assets/images' },
      ],
    }),
  ],
  devServer: {
    static: path.resolve(__dirname, 'src'),
  },
};
